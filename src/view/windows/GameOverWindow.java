package view.windows;

import controller.game.Control;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * This class creates and runs the window of the game when the game is over.
 * Furthermore it interacts with the Model classes through Control class for example
 * for saving final scores.
 * 
 * @author AURORA ROMANELLI, FRANCESCO VITALI
 */
public class GameOverWindow extends JFrame {

  private static final long serialVersionUID = 1L;

  /**
  * Set the width of the window.
  */
  public static final int SCREEN_WIDTH = 475;
  private final JTextField text = new JTextField(10);
  private final JLabel label = new JLabel("Hey loser, write here your name: ");
  private final JTextArea ranking = new JTextArea(11,40);
  private final JButton savePlayerNameButton = new JButton("Enter");
  private final JButton playAgainButton = new JButton("Play again");
  private final JButton closeAllButton = new JButton("Exit");



  /**
   * Create and manage the window of the game.
   * @param control controller of the game.
   */
  public GameOverWindow(final Control control) {

    super("Game Over");
    setSize(SCREEN_WIDTH, 310);
    setLocation(450, 250);
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    setResizable(false);
    setVisible(true);
    setLayout(new FlowLayout(FlowLayout.CENTER));
    
    ranking.setEditable(false);
    ranking.setText(control.getScoreManager().getRanking().getHighscoreString());

    add(label);
    add(text);
    add(savePlayerNameButton);
    add(ranking);
    add(playAgainButton);
    add(closeAllButton);
    
    savePlayerNameButton.addActionListener(e -> {
      control.addToRanking(text.getText());
      savePlayerNameButton.setEnabled(false);
      ranking.setText(control.getScoreManager().getRanking().getHighscoreString());
    });
    
    playAgainButton.addActionListener(e -> {
      this.dispose();
      control.gameReset();
      control.gameStart();
    });

    closeAllButton.addActionListener(e -> {
      System.exit(0);
    });
    
  }
  
}