package util;

/**
 * This Enumeration is used to manage the different difficulties of the fixedSpeed mode.
 * @author ROMANELLI AURORA
 */
public enum Difficulty {

/**
 * low difficulty.
 */
EASY,

/**
 * medium difficulty.
 */

MEDIUM,

/**
 *  high difficulty.
 */

HARD
}
