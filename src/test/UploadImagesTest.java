package test;

import static org.junit.Assert.assertTrue;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.Test;

/**
 * This Class do some test to upload images of the game.
 * 
 * @author POGGI GIOVANNI
 */
public class UploadImagesTest {

  /**
   * This method try the upload of image.
   * 
   * @param image of the game to try upload.
   * @return true if the image exist, false otherwise.
   */
  private boolean tryUpload(final BufferedImage image) {
    return image != null;
  }

  /**
   * Test the upload of the images of the game.
   * @throws IOException if something goes wrong.
   */
  @Test
  public void testUploadingImage() throws IOException {
    BufferedImage firstSpriteOfLand = ImageIO.read(this.getClass().getResourceAsStream("res/game/environment/land1.png"));
    BufferedImage secondSpriteOfLand = ImageIO.read(this.getClass().getResourceAsStream("res/game/environment/land2.png"));
    BufferedImage thirdSpriteOfLand = ImageIO.read(this.getClass().getResourceAsStream("res/game/environment/land3.png"));
    BufferedImage cloud = ImageIO.read(this.getClass().getResourceAsStream("res/game/environment/cloud.PNG"));
    BufferedImage cactus1 = ImageIO.read(this.getClass().getResourceAsStream("res/game/enemiesManager/cactus1.png"));
    BufferedImage cactus2 = ImageIO.read(this.getClass().getResourceAsStream("res/game/enemiesManager/cactus2.png"));
    assertTrue(tryUpload(firstSpriteOfLand));
    assertTrue(tryUpload(secondSpriteOfLand));
    assertTrue(tryUpload(thirdSpriteOfLand));
    assertTrue(tryUpload(cloud));
    assertTrue(tryUpload(cactus1));
    assertTrue(tryUpload(cactus2));
  }
}