package test;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

import game.score.HighScore;
import game.score.RankingManager;


/**
 * @author ROMANELLI AURORA
 *
 */
public class GameDataTest {

	private ArrayList<HighScore> scores;
	RankingManager score = new RankingManager();
	
	private void initializeList() {
		scores =score.getScores();
		System.out.println(scores.toString());
	}
	
	private void clear() {
		score.clear();
	}
	
	private boolean existFile() {
		return score.isPresent();
	}
	
	/**
	 * 
	 */
	@Test
	public void testGameData() {
		initializeList();
		Assert.assertTrue(scores!=null);
		clear();
		Assert.assertFalse(existFile());
		
	}
}
