package game.score;

import java.io.Serializable;

/**
 * Class that create object used to memorize the name and score of player in the ranking.
 * @author ROMANELLI AURORA
 *
 */
public class HighScore implements Serializable {

  private static final long serialVersionUID = -1816198632672400661L;
  private static final int MAX_STRING = 10;
  private String name;
  private final int score;

  /**
 * Constructor.
 * @param n name of player
 * @param s score of player.
 */
  public HighScore(final String n, final int s) {
    if (n.length() > MAX_STRING) {
      name = n.substring(0, MAX_STRING - 1);
    } else {
      name = n;
    }
    score = s;
  }

  @Override
  public String toString() {
    return name + " has scored: " + score;
  }

  /**
   * Getter.
 * @return Score
 */
  public int getScore() {
    return score;
  }

  /**
   * Getter.
 * @return Name
 */
  public String getName() {
    return name;
  }
}
