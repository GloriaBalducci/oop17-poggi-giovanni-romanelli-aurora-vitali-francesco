package game.speed;

import util.Difficulty;

/**
 * This Class manages the game mode with the speed set by choosing between 3 different difficulties.
 * Present empty method due to his fixed nature.
 * @author ROMANELLI AURORA
 *
 */
public class FixedSpeed implements SpeedManager {

    
  private double speed;
  private final Difficulty diff;

  /**
   * Constructor.
   * @param difficulty difficulty of game.
   */
  public FixedSpeed(final Difficulty difficulty) {
    super();
    this.diff = difficulty;
    switch (diff) {
      case EASY: this.speed = 4;
        break;

      case MEDIUM: this.speed = 7;
        break;

      case HARD: this.speed = 10;
        break;
      default:
        break;
    }
  }

  @Override
  public void updateSpeed() {
  }

  @Override
  public double getSpeed() {
    return speed;
  }

  @Override
  public void reset() {
  }

}
