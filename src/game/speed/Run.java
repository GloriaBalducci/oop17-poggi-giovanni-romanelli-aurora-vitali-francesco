package game.speed;

import controller.game.Control;

/**
 * This class contains the main method and manage game engine.
 * 
 * @author Romanelli Aurora
 *
 */
public class Run implements Runnable {


  private volatile Thread thread;
  private final Control control;

  /**
   * Constructor.
   * @param control 
   * 
   * 
   */

  public Run(final Control control) {
    super();
    this.control = control;
  }
  

  /**
   * Start the game thread.
   */
  public void startGame() {
    thread = new Thread(this);
    thread.start();
  }

  /**
  * Stop the game thread.
  */
  public void stopGame() {
    thread = null;
  }

  @Override
  public void run() {
    final int fps = 100;
    final long msPerFrame = 1000 * 1000000 / fps;
    long lastTime = 0;
    long elapsed;
    int msSleep;
    int nanoSleep;
    final Thread thisThread = Thread.currentThread();
    while (thread == thisThread) {
      control.gameUpdate();
      control.repaint();
      elapsed = (lastTime + msPerFrame - System.nanoTime());
      msSleep = (int) (elapsed / 1000000);
      nanoSleep = (int) (elapsed % 1000000);
      if (msSleep <= 0) {
        lastTime = System.nanoTime();
        continue;
      }
      try {
        Thread.sleep(msSleep, nanoSleep);
      } catch (final InterruptedException e) {
        e.printStackTrace();
      }
      lastTime = System.nanoTime();
    }
  }

}
