package game.mainCharacter;

/**
 * This enum class lists the several states of the main character during the game.
 * 
 * @author FRANCESCO VITALI
 */
public enum MainCharacterState {
    
    /**
     * Main character's state when he is running normally.
     */
    NORMAL_RUN,
    
    /**
     * Main character's state when he is jumping.
     */
    JUMPING,
    
    /**
     * Main character's state when he is running with the head down.
     */
    DOWN_RUN,
    
    /**
     * Main character's state when he is dead.
     */
    DEAD;

}
